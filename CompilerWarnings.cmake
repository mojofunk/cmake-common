function(get_target_warnings OUT_ARG_WARNINGS IN_ARG_WARNINGS_AS_ERRORS)

  set(MSVC_WARNINGS
      /W4 # Baseline reasonable warnings
      /wd4316 # An over-aligned object allocated by using operator new may not
              # have the specified alignment. For moodycamel::ReaderWriterQueue.
      /w14242 # 'identfier': conversion from 'type1' to 'type1', possible loss
              # of data
      /w14254 # 'operator': conversion from 'type1:field_bits' to
              # 'type2:field_bits', possible loss of data
      /w14263 # 'function': member function does not override any base class
              # virtual member function
      /w14265 # 'classname': class has virtual functions, but destructor is not
              # virtual instances of this class may not be destructed correctly
      /w14287 # 'operator': unsigned/negative constant mismatch
      /we4289 # nonstandard extension used: 'variable': loop control variable
              # declared in the for-loop is used outside the for-loop scope
      /w14296 # 'operator': expression is always 'boolean_value'
      /w14311 # 'variable': pointer truncation from 'type1' to 'type2'
      /w14545 # expression before comma evaluates to a function which is missing
              # an argument list
      /w14546 # function call before comma missing argument list
      /w14547 # 'operator': operator before comma has no effect; expected
              # operator with side-effect
      /w14549 # 'operator': operator before comma has no effect; did you intend
              # 'operator'?
      /w14555 # expression has no effect; expected expression with side- effect
      /w14619 # pragma warning: there is no warning number 'number'
      /w14640 # Enable warning on thread un-safe static member initialization
      /w14826 # Conversion from 'type1' to 'type_2' is sign-extended. This may
              # cause unexpected runtime behavior.
      /w14905 # wide string literal cast to 'LPSTR'
      /w14906 # string literal cast to 'LPWSTR'
      /w14928 # illegal copy-initialization; more than one user-defined
              # conversion has been implicitly applied
      /permissive- # Visual Studio 2017 and later to specify standards-conforming
                   # compiler behavior. Somewhat equivalent to -Wpedantic
  )

  set(CLANG_BASE_WARNINGS
      -Wall
      -Wno-unknown-pragmas
      -Wextra # reasonable and standard
#       -Wshadow # warn the user if a variable declaration shadows one from a
#                # parent context
      -Wnon-virtual-dtor # warn the user if a class with virtual functions has a
                         # non-virtual destructor. This helps catch hard to
                         # track down memory errors
#       -Wold-style-cast # warn for c-style casts
      -Wcast-align # warn for potential performance problem casts
      -Wunused # warn on anything being unused
      -Woverloaded-virtual # warn if you overload (not override) a virtual
                           # function
      -Wpedantic # warn if non-standard C++ is used
      -Wconversion # warn on type conversions that may lose data
#       -Wnull-dereference # warn if a null dereference is detected
#       -Wdouble-promotion # warn if float is implicit promoted to double # warning in fmt headers, re-enable on fmt upgrade
      -Wformat=2 # warn on security issues around functions that format output
                 # (ie printf)
  )

  set(CLANG_WARNINGS
      ${CLANG_BASE_WARNINGS}
      -Wno-deprecated-copy # disable this -Wextra warning for now # This isn't supported by AppleClang
  )

  set(APPLE_CLANG_WARNINGS
      ${CLANG_BASE_WARNINGS}
#      -Wno-deprecated # disable this -Wextra warning for now
  )

  if(IN_ARG_WARNINGS_AS_ERRORS)
    set(CLANG_WARNINGS ${CLANG_WARNINGS} -Werror)
    set(MSVC_WARNINGS ${MSVC_WARNINGS} /WX)
  endif()

  set(GCC_WARNINGS
      ${CLANG_WARNINGS}
      -Wno-aligned-new # Added for the same reason as /wd4316, should not be required
                       # with c++17 enabled
      -Wmisleading-indentation # warn if indentation implies blocks where blocks
                               # do not exist
      -Wduplicated-cond # warn if if / else chain has duplicated conditions
      -Wduplicated-branches # warn if if / else branches have duplicated code
      -Wlogical-op # warn about logical operations being used where bitwise were
                   # probably wanted
#       -Wuseless-cast # warn if you perform a cast to the same type
  )

  if(MSVC)
    set(${OUT_ARG_WARNINGS} ${MSVC_WARNINGS} PARENT_SCOPE)
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    set(${OUT_ARG_WARNINGS} ${CLANG_WARNINGS} PARENT_SCOPE)
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
    set(${OUT_ARG_WARNINGS} ${APPLE_CLANG_WARNINGS} PARENT_SCOPE)
  else()
    set(${OUT_ARG_WARNINGS} ${GCC_WARNINGS} PARENT_SCOPE)
  endif()

endfunction()
